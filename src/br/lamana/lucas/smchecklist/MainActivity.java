package br.lamana.lucas.smchecklist;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import br.lamana.lucas.smchecklist.database.CompraDAO;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.util.Constantes;
import br.lamana.lucas.smchecklist.util.DateUtils;
import br.lamana.lucas.smcheklist.R;

@SuppressLint("InflateParams")
public class MainActivity extends ActionBarActivity {

  /**
   * ListView para as opções
   */
  private ListView listViewOpcoes;

  // Opções do aplicativo
  private static final String OPCAO_NOVA_LISTA = "Nova lista";
  private static final String OPCAO_LISTA_EXISTENTE = "Gerenciar listas";
  private static final String OPCAO_INFO_LISTAS = "Informações legais";

  /**
   * DAO (Data access object) para acesso e manipulação do banco de dados.
   */
  private CompraDAO compraDAO;

  /**
   * ImageView com o logo da aplicação.
   */
  private ImageView imageViewCarrinho;

  /**
   * Lista de compras.
   */
  private List<Compra> compras;

  // Drawer Layout
  private DrawerLayout drawerLayout;
  private ActionBarDrawerToggle actionBarDrawerToggle;
  private ActionBar actionBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    compraDAO = new CompraDAO();
    compraDAO.abrir(this);

    compras = new ArrayList<Compra>();
    compras = compraDAO.listar();

    imageViewCarrinho = (ImageView) findViewById(R.id.home_imageViewCarrinho);
    imageViewCarrinho.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        novaLista();
      }
    });

    actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(true);

    final String[] opcoes = new String[] {
        OPCAO_NOVA_LISTA,
        OPCAO_LISTA_EXISTENTE,
        OPCAO_INFO_LISTAS
    };

    listViewOpcoes = (ListView) findViewById(R.id.main_listViewOpcoes);
    listViewOpcoes.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
        opcoes));

    listViewOpcoes.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        String opcaoEscolhida = opcoes[position];

        if (opcaoEscolhida.equals(OPCAO_NOVA_LISTA)) {
          novaLista();
        }
        else if (opcaoEscolhida.equals(OPCAO_LISTA_EXISTENTE)) {
          startActivity(new Intent(MainActivity.this, VisualizarListaActivity.class));
        }
        else if (opcaoEscolhida.equals(OPCAO_INFO_LISTAS)) {
          if (compras.size() < 1) {
            Toast.makeText(MainActivity.this, R.string.main_toast_naoHaLista,
                Toast.LENGTH_SHORT).show();
          }
          else {
            startActivity(new Intent(MainActivity.this, InfoActivity.class));
          }
        }
      }
    });

    drawerLayout = (DrawerLayout) findViewById(R.id.main_drawerLayout);
    actionBarDrawerToggle = new ActionBarDrawerToggle(
        this,
        drawerLayout,
        R.drawable.ic_navigation_drawer,
        R.string.main_drawerLayout_aberto,
        R.string.main_drawerLayout_fechado) {

      @Override
      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
      }

      @Override
      public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
      }
    };

    drawerLayout.setDrawerListener(actionBarDrawerToggle);
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    actionBarDrawerToggle.syncState();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onDestroy() {
    compraDAO.fechar();
    super.onDestroy();
  }

  public void novaLista() {
    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
    View v = inflater.inflate(R.layout.dialog_edit_text, null);

    final EditText editTextTitulo = (EditText) v
        .findViewById(R.id.dialog_editTextTitulo);

    final EditText editTextValor = (EditText) v
        .findViewById(R.id.dialog_editTextValor);

    new AlertDialog.Builder(MainActivity.this)
        .setTitle(R.string.dialog_editText_novaCompra)
        .setView(v)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();

            String titulo = editTextTitulo.getText().toString();
            try {
              double valorLimite = Double.parseDouble(editTextValor.getText().toString());

              if (titulo != null && !titulo.equals("") && valorLimite != 0) {
                if (compraDAO.inserir(titulo, DateUtils.dateFormat.format(date), valorLimite)) {
                  startActivity(new Intent(MainActivity.this, NovaListaActivity.class).putExtra(
                      Constantes.EXTRA_COMPRA, compraDAO.getUltimaCompraInserida()));
                }
              }
              else {
                Toast.makeText(MainActivity.this, R.string.main_toast_listaSemTitulo,
                    Toast.LENGTH_SHORT).show();
              }
            }
            catch (NumberFormatException nfe) {
              Toast.makeText(MainActivity.this, R.string.main_toast_digiteUmNumero, Toast.LENGTH_SHORT).show();
            }
          }
        })
        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {

          }
        })
        .show();
  }

}
