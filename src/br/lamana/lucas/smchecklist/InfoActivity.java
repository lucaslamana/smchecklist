package br.lamana.lucas.smchecklist;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import br.lamana.lucas.smchecklist.database.CompraDAO;
import br.lamana.lucas.smchecklist.database.ItemDAO;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.model.Item;
import br.lamana.lucas.smchecklist.util.Constantes;
import br.lamana.lucas.smchecklist.util.Util;
import br.lamana.lucas.smcheklist.R;

public class InfoActivity extends ActionBarActivity implements OnClickListener {

  /**
   * TextView com a compra mais cara.
   */
  private TextView textViewCompraMaisCara;

  /**
   * TextView label de compra mais cara.
   */
  private TextView textViewCompraMaisCaraLabel;

  /**
   * TextView com mais informações sobre a compra mais cara.
   */
  private TextView textViewCompraMaisCaraInfo;

  /**
   * TextView com o item mais caro.
   */
  private TextView textViewItemMaisCaro;

  /**
   * TextView label de item mais caro.
   */
  private TextView textViewItemMaisCaroLabel;

  /**
   * TextView com mais informações sobre o item mais caro.
   */
  private TextView textViewItemMaisCaroInfo;

  /**
   * TextView com a compra com mais itens.
   */
  private TextView textViewCompraComMaisItens;

  /**
   * TextView label de compra com mair itens.
   */
  private TextView textViewCompraComMaisItensLabel;

  /**
   * TextView com mais informações sobre a compra com mais itens.
   */
  private TextView textViewCompraComMaisItensInfo;

  /**
   * TextView com o Item com maior quantidade em uma compra.
   */
  private TextView textViewItemComMaiorQuantidade;

  /**
   * TextView label de item com a maior quantidade em uma compra.
   */
  private TextView textViewItemComMaiorQuantidadeLabel;

  /**
   * TextView com mais informações sobre o item com maior quantidade em uma
   * compra.
   */
  private TextView textViewItemComMaiorQuantidadeInfo;

  /**
   * DAO de item para a manipualação do banco de dados.
   */
  private ItemDAO itemDAO;

  /**
   * DAO de compra para a manipulação do banco de dados.
   */
  private CompraDAO compraDAO;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    itemDAO = new ItemDAO();
    itemDAO.abrir(this);

    compraDAO = new CompraDAO();
    compraDAO.abrir(this);

    // Label.
    textViewCompraMaisCaraLabel = (TextView) findViewById(R.id.info_textViewCompraMaisCaraLabel);
    textViewCompraMaisCaraLabel.setOnClickListener(this);

    textViewCompraMaisCara = (TextView) findViewById(R.id.info_textViewCompraMaisCara);
    textViewCompraMaisCara.setText(getCompraMaisCara().getTitulo());

    // Info.
    textViewCompraMaisCaraInfo = (TextView) findViewById(R.id.info_textViewCompraMaisCaraInfo);
    textViewCompraMaisCaraInfo.setText(infoCompra(getCompraMaisCara()));

    // Label.
    textViewItemMaisCaroLabel = (TextView) findViewById(R.id.info_textViewItemMaisCaroLabel);
    textViewItemMaisCaroLabel.setOnClickListener(this);

    textViewItemMaisCaro = (TextView) findViewById(R.id.info_textViewItemMaisCaro);
    textViewItemMaisCaro.setText(getItemMaisCaro().getDescricao());

    // Info.
    textViewItemMaisCaroInfo = (TextView) findViewById(R.id.info_textViewItemMaisCaroInfo);
    textViewItemMaisCaroInfo.setText(infoItem(getItemMaisCaro()));

    // Label.
    textViewCompraComMaisItensLabel = (TextView) findViewById(R.id.info_textViewCompraComMaisItensLabel);
    textViewCompraComMaisItensLabel.setOnClickListener(this);

    textViewCompraComMaisItens = (TextView) findViewById(R.id.info_textViewCompraComMaisItens);
    textViewCompraComMaisItens.setText(getCompraComMaisItens().getTitulo());

    // Info.
    textViewCompraComMaisItensInfo = (TextView) findViewById(R.id.info_textViewCompraComMaisItensInfo);
    textViewCompraComMaisItensInfo.setText(infoCompra(getCompraComMaisItens()));

    // Label.
    textViewItemComMaiorQuantidadeLabel = (TextView) findViewById(R.id.info_textViewItemComMaiorQuantidadeLabel);
    textViewItemComMaiorQuantidadeLabel.setOnClickListener(this);

    textViewItemComMaiorQuantidade = (TextView) findViewById(R.id.info_textViewItemComMaiorQuantidade);
    textViewItemComMaiorQuantidade.setText(getItemMaiorQuantidade().getDescricao());

    // Info.
    textViewItemComMaiorQuantidadeInfo = (TextView) findViewById(R.id.info_textViewItemComMaiorQuantidadeInfo);
    textViewItemComMaiorQuantidadeInfo.setText(infoItem(getItemMaiorQuantidade()));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.info, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    return super.onOptionsItemSelected(item);
  }

  /**
   * Pega a compra mais cara do banco de dados.
   * 
   * @return Compra mais cara.
   */
  public Compra getCompraMaisCara() {
    Compra compraMaisCara = null;
    double valorAtual = 0;
    double valorMaior = 0;
    List<Item> itens;

    for (Compra compra : compraDAO.listar()) {
      // Para cada compra, lista os itens da mesma.
      itens = itemDAO.listarItensDeCompra(compra.getId());

      // Pega o valor total da compra
      valorAtual = Util.getValorTotal(itens);

      if (valorAtual > valorMaior) {
        valorMaior = valorAtual;
        compraMaisCara = compra;
      }
    }

    return compraMaisCara;
  }

  /**
   * Pega a compra com mais itens do banco de dados.
   * 
   * @return Compra com mais itens.
   */
  public Compra getCompraComMaisItens() {
    Compra compraMaisItens = null;
    double valorAtual = 0;
    double valorMaior = 0;
    List<Item> itens;

    for (Compra compra : compraDAO.listar()) {
      itens = itemDAO.listarItensDeCompra(compra.getId());

      valorAtual = Util.getQuantidadeTotal(itens);

      if (valorAtual > valorMaior) {
        valorMaior = valorAtual;
        compraMaisItens = compra;
      }
    }

    return compraMaisItens;
  }

  /**
   * Pega o item mais caro no banco de dados.
   * 
   * @return Item mais caro.
   */
  public Item getItemMaisCaro() {
    Item itemMaisCaro = null;
    double valorAtual = 0;
    double valorMaior = 0;

    for (Item item : itemDAO.listar()) {
      valorAtual = item.getValor();

      if (valorAtual > valorMaior) {
        valorMaior = valorAtual;
        itemMaisCaro = item;
      }
    }

    return itemMaisCaro;
  }

  /**
   * Pega o item com maior quantidade no banco de dados.
   * 
   * @return Item com maior quantidade.
   */
  public Item getItemMaiorQuantidade() {
    Item itemMaiorQuantidade = null;
    double valorAtual = 0;
    double valorMaior = 0;

    for (Item item : itemDAO.listar()) {
      valorAtual = item.getQuantidade();

      if (valorAtual > valorMaior) {
        valorMaior = valorAtual;
        itemMaiorQuantidade = item;
      }
    }

    return itemMaiorQuantidade;
  }

  @Override
  public void onClick(View v) {
    if (v == textViewCompraMaisCaraLabel) {
      startActivity(new Intent(this, ListaCompraActivity.class).putExtra(Constantes.EXTRA_COMPRA,
          getCompraMaisCara()));
    }
    else if (v == textViewItemMaisCaroLabel) {
      Compra compra = compraDAO.getCompraPeloId(getItemMaisCaro().getCompra());
      startActivity(new Intent(this, ListaCompraActivity.class).putExtra(Constantes.EXTRA_COMPRA,
          compra));
    }
    else if (v == textViewCompraComMaisItensLabel) {
      startActivity(new Intent(this, ListaCompraActivity.class).putExtra(Constantes.EXTRA_COMPRA,
          getCompraComMaisItens()));
    }
    else if (v == textViewItemComMaiorQuantidadeLabel) {
      Compra compra = compraDAO.getCompraPeloId(getItemMaiorQuantidade().getCompra());
      startActivity(new Intent(this, ListaCompraActivity.class).putExtra(Constantes.EXTRA_COMPRA,
          compra));
    }
  }

  @Override
  protected void onDestroy() {
    itemDAO.fechar();
    compraDAO.fechar();
    super.onDestroy();
  }

  /**
   * Retorna algumas informações sobre a compra.
   * 
   * @param compra
   *          Compra.
   * 
   * @return Valor total e quantidade de itens da compra.
   */
  public String infoCompra(Compra compra) {
    List<Item> itens = new ArrayList<Item>();
    itens = itemDAO.listarItensDeCompra(compra.getId());

    double valorTotal = Util.getValorTotal(itens);
    int quantidadeItens = Util.getQuantidadeTotal(itens);

    StringBuilder s = new StringBuilder();
    s.append("(" + quantidadeItens + " itens, R$");
    s.append(String.format("%.2f", valorTotal) + ")");

    return s.toString();
  }

  /**
   * Retorna algumas informações sobre o Item.
   * 
   * @param item
   *          Item.
   * 
   * @return Valor e em de qual compra o item pertence.
   */
  public String infoItem(Item item) {
    double valorItem = item.getValor();
    String nomeCompra = compraDAO.getCompraPeloId(item.getCompra()).getTitulo();

    StringBuilder s = new StringBuilder();
    s.append(String.format("(R$ %.2f", valorItem));
    s.append(", na compra " + nomeCompra + ")");

    return s.toString();
  }

}
