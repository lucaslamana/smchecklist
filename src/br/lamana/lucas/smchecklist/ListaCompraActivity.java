package br.lamana.lucas.smchecklist;

import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import br.lamana.lucas.smchecklist.database.CompraDAO;
import br.lamana.lucas.smchecklist.database.ItemDAO;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.model.Item;
import br.lamana.lucas.smchecklist.util.Constantes;
import br.lamana.lucas.smchecklist.util.Util;
import br.lamana.lucas.smcheklist.R;

public class ListaCompraActivity extends ActionBarActivity {

  /**
   * TextView com o título da compra.
   */
  private TextView textViewTitulo;

  /**
   * TextView com a data da compra.
   */
  private TextView textViewData;

  /**
   * TextView com a quantidade de itens da compra.
   */
  private TextView textViewQuantidade;

  /**
   * TextView com o valor total da compra.
   */
  private TextView textViewValor;

  /**
   * ListView com os itens da compra.
   */
  private ListView listViewItens;

  /**
   * Lista de itens.
   */
  private List<Item> itens;

  /**
   * DAO de compra para o acesso e manipulação do banco de dados.
   */
  private CompraDAO compraDAO;

  /**
   * DAO de item para o acesso e manipulação do banco de dados.
   */
  private ItemDAO itemDAO;

  /**
   * Objeto compra que será recebido por extra.
   */
  private Compra compra;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista_compra);

    compra = (Compra) getIntent().getSerializableExtra(Constantes.EXTRA_COMPRA);

    compraDAO = new CompraDAO();
    compraDAO.abrir(this);

    itemDAO = new ItemDAO();
    itemDAO.abrir(this);

    itens = itemDAO.listarItensDeCompra(compra.getId());

    textViewTitulo = (TextView) findViewById(R.id.listaCompra_textViewTitulo);
    textViewTitulo.setText(compra.getTitulo());

    textViewData = (TextView) findViewById(R.id.listaCompra_textViewData);
    textViewData.setText(compra.getData());

    textViewQuantidade = (TextView) findViewById(R.id.listaCompra_textViewQuantidade);
    textViewQuantidade.setText(String.format("%d itens,", Util.getQuantidadeTotal(itens)));

    // Formata o valor para mostrar duas casa decimais após a vírgula.
    textViewValor = (TextView) findViewById(R.id.listaCompra_textViewValor);
    textViewValor.setText(String.format("R$ %.2f", Util.getValorTotal(itens)));

    AdptadorItens adptadorItens = new AdptadorItens(this, itens);

    listViewItens = (ListView) findViewById(R.id.listaCompra_listViewItens);
    listViewItens.setAdapter(adptadorItens);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.lista_compra, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.

    return super.onOptionsItemSelected(item);
  }

}
