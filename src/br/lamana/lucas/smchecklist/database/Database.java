package br.lamana.lucas.smchecklist.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

  /**
   * Nome do banco de dados.
   */
  public static final String DATABASE_NAME = "smchecklist";

  /**
   * Versão do bando de dados.
   */
  public static final int DATABASE_VERSION = 3;

  /**
   * Tabela de compras. Usado como uma constante estática para poder acessar de
   * outras classes, uma vez que facilita na digitação da mesma e inibi
   * possiveis erros.
   */
  public static final String TABELA_COMPRA = "compra";

  /**
   * Tabela de itens. Idem {@link #TABELA_COMPRA}
   */
  public static final String TABELA_ITEM = "item";

  /**
   * Cria um novo helper para manipulação do banco de dados local (banco
   * {@link #DATABASE_NAME}, versão {@link #DATABASE_VERSION}).
   * 
   * @param context
   *          Usado para abrir ou criar a base de dados.
   */
  public Database(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    // Cria a tabela de compras
    db.execSQL("" +
        "create table " + TABELA_COMPRA + "(" +
        "id integer primary key not null, " +
        "titulo text not null, " +
        "data text not null, " +
        "valor real not null);");

    // Cria a tabela de itens
    db.execSQL("" +
        "create table " + TABELA_ITEM + "(" +
        "id integer primary key not null, " +
        "descricao text not null, " +
        "valor real not null, " +
        "quantidade integer not null, " +
        "compra_id integer not null);");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // Dropa as tabelas para upgrade do banco.
    db.execSQL("drop table " + TABELA_COMPRA);
    db.execSQL("drop table " + TABELA_ITEM);

    // Recria as tabelas.
    onCreate(db);
  }

}
