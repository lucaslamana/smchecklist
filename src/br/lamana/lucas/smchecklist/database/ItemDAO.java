package br.lamana.lucas.smchecklist.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.model.Item;

public class ItemDAO {

  private Database database;
  private SQLiteDatabase sqLiteDatabase;

  public ItemDAO() {
  }

  /**
   * Cria uma instância do banco de dados para execução de ações dentro do
   * mesmo.
   * 
   * @param context
   */
  public void abrir(Context context) {
    database = new Database(context);
    sqLiteDatabase = database.getWritableDatabase();
  }

  /**
   * Fecha a instância do banco de dados.
   */
  public void fechar() {
    database.close();
  }

  /**
   * Insere um item de uma compra no banco de dados.
   * 
   * @param descricao
   *          Descrição do {@link Item}.
   * @param valor
   *          Valor do {@link Item}.
   * @param quantidade
   *          Quantidade do {@link Item}
   * @param compra
   *          Id da {@link Compra} em que o {@link Item} está.
   * 
   * @return <b>true</b> se foi inserido,<br>
   *         <b>false</b> se não foi possível inserir.
   */
  public boolean inserir(String descricao, double valor, int quantidade, int compra) {
    ContentValues values = new ContentValues();
    values.put("descricao", descricao);
    values.put("valor", valor);
    values.put("quantidade", quantidade);
    values.put("compra_id", compra);

    long insert = sqLiteDatabase.insert(Database.TABELA_ITEM, null, values);

    if (insert > 0) {
      return true;
    }

    return false;
  }

  /**
   * Deleta um item do banco de dados.
   * 
   * @param id
   *          Id do {@link Item}.
   * 
   * @return <b>true</b> se foi excluído,<br>
   *         <b>false</b> se não foi possível excluir.
   */
  public boolean deletar(int id) {
    int delete = sqLiteDatabase.delete(Database.TABELA_ITEM, "id = " + id, null);
    if (delete > 0) {
      return true;
    }

    return false;
  }

  /**
   * Deleta todos os itens de uma compra.
   * 
   * @param compraId
   *          Id da compra.
   * @return <b>true</b> se foi excluído,<br>
   *         <b>false</b> se não foi possível excluir.
   */
  public boolean deletarItensDeCompra(int compraId) {
    int delete = sqLiteDatabase.delete(Database.TABELA_ITEM, "compra_id = " + compraId, null);
    if (delete > 0) {
      return true;
    }

    return false;
  }

  /**
   * Lista de itens exitentes no banco de dados.
   * 
   * @return lista de {@link Item}.
   */
  public List<Item> listar() {
    List<Item> itens = new ArrayList<Item>();

    Cursor cursor = sqLiteDatabase.query(Database.TABELA_ITEM, null, null, null, null, null, null);
    if (cursor.moveToFirst()) {
      int colunaId = cursor.getColumnIndex("id");
      int colunaDescricao = cursor.getColumnIndex("descricao");
      int colunaValor = cursor.getColumnIndex("valor");
      int colunaQuantidade = cursor.getColumnIndex("quantidade");
      int colunaCompra = cursor.getColumnIndex("compra_id");

      do {
        int id = cursor.getInt(colunaId);
        String descricao = cursor.getString(colunaDescricao);
        double valor = cursor.getDouble(colunaValor);
        int quantidade = cursor.getInt(colunaQuantidade);
        int compra = cursor.getInt(colunaCompra);

        itens.add(new Item(id, descricao, valor, quantidade, compra));
      }
      while (cursor.moveToNext());
    }

    return itens;
  }

  /**
   * Lista de itens exitentes no banco de dados referentes a uma compra.
   * 
   * @return lista de {@link Item} de uma {@link Compra}.
   */
  public List<Item> listarItensDeCompra(int id) {
    List<Item> itens = new ArrayList<Item>();

    Cursor cursor = sqLiteDatabase.query(Database.TABELA_ITEM, null, "compra_id = " + id, null,
        null, null, null);
    if (cursor.moveToFirst()) {
      int colunaId = cursor.getColumnIndex("id");
      int colunaDescricao = cursor.getColumnIndex("descricao");
      int colunaValor = cursor.getColumnIndex("valor");
      int colunaQuantidade = cursor.getColumnIndex("quantidade");
      int colunaCompra = cursor.getColumnIndex("compra_id");

      do {
        int _id = cursor.getInt(colunaId);
        String descricao = cursor.getString(colunaDescricao);
        double valor = cursor.getDouble(colunaValor);
        int quantidade = cursor.getInt(colunaQuantidade);
        int compra = cursor.getInt(colunaCompra);

        itens.add(new Item(_id, descricao, valor, quantidade, compra));
      }
      while (cursor.moveToNext());
    }

    return itens;
  }
}
