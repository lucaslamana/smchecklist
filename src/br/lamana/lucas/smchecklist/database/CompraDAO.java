package br.lamana.lucas.smchecklist.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.lamana.lucas.smchecklist.model.Compra;

public class CompraDAO {

  private Database database;
  private SQLiteDatabase sqLiteDatabase;

  public CompraDAO() {
    // TODO Auto-generated constructor stub
  }

  /**
   * Cria uma instância do banco de dados para execução de ações dentro do
   * mesmo.
   * 
   * @param context
   */
  public void abrir(Context context) {
    database = new Database(context);
    sqLiteDatabase = database.getWritableDatabase();
  }

  /**
   * Fecha a instância do banco de dados.
   */
  public void fechar() {
    database.close();
  }

  /**
   * Procura por uma compra no banco de dados com o id passado por parâmetro.
   * 
   * @param id
   *          id da {@link Compra}
   * @return Compra.
   */
  public Compra getCompraPeloId(int id) {
    Compra compra = null;

    Cursor cursor = sqLiteDatabase.query(Database.TABELA_COMPRA, null, "id = " + id, null, null,
        null, null);
    if (cursor.moveToFirst()) {
      int _id = cursor.getInt(cursor.getColumnIndex("id"));
      String titulo = cursor.getString(cursor.getColumnIndex("titulo"));
      String data = cursor.getString(cursor.getColumnIndex("data"));
      double valorLimite = cursor.getDouble(cursor.getColumnIndex("valor"));

      compra = new Compra(_id, titulo, data, valorLimite);
    }
    return compra;
  }

  /**
   * Pega a última compra inserida.
   * 
   */
  public Compra getUltimaCompraInserida() {
    Compra compra = null;

    Cursor cursor = sqLiteDatabase.rawQuery("" +
        "select * from " + Database.TABELA_COMPRA + " order by id desc limit 1", null);

    if (cursor.moveToFirst()) {
      int _id = cursor.getInt(cursor.getColumnIndex("id"));
      String titulo = cursor.getString(cursor.getColumnIndex("titulo"));
      String data = cursor.getString(cursor.getColumnIndex("data"));
      double valorLimite = cursor.getDouble(cursor.getColumnIndex("valor"));

      compra = new Compra(_id, titulo, data, valorLimite);
    }
    return compra;
  }

  /**
   * Insere uma compra no banco de dados.
   * 
   * @param titulo
   *          título da {@link Compra}.
   * 
   * @return <b>true</b> se foi inserido,<br>
   *         <b>false</b> se não foi possível inserir.
   */
  public boolean inserir(String titulo, String data, double valorLimite) {
    ContentValues values = new ContentValues();
    values.put("titulo", titulo);
    values.put("data", data);
    values.put("valor", valorLimite);

    long insert = sqLiteDatabase.insert(Database.TABELA_COMPRA, null, values);

    if (insert > 0) {
      return true;
    }

    return false;
  }

  /**
   * Deleta uma compra no banco de dados.
   * 
   * @param id
   *          id da {@link Compra} a ser excluída.
   * 
   * @return <b>true</b> se foi excluída,<br>
   *         <b>false</b> se não foi possível excluir.
   */
  public boolean deletar(int id) {
    int delete = sqLiteDatabase.delete(Database.TABELA_COMPRA, "id = " + id, null);

    if (delete > 0) {
      return true;
    }

    return false;
  }

  /**
   * Lista as compras existentes no banco de dados.
   * 
   * @return lista de {@link Compra}
   */
  public List<Compra> listar() {
    List<Compra> compras = new ArrayList<Compra>();

    Cursor cursor = sqLiteDatabase
        .query(Database.TABELA_COMPRA, null, null, null, null, null, null);
    if (cursor.moveToFirst()) {
      int colunaId = cursor.getColumnIndex("id");
      int colunaTitulo = cursor.getColumnIndex("titulo");
      int colunaData = cursor.getColumnIndex("data");
      int colunaValorLimite = cursor.getColumnIndex("valor");

      do {
        int id = cursor.getInt(colunaId);
        String titulo = cursor.getString(colunaTitulo);
        String data = cursor.getString(colunaData);
        double valorLimite = cursor.getDouble(colunaValorLimite);

        compras.add(new Compra(id, titulo, data, valorLimite));

      }
      while (cursor.moveToNext());
    }

    return compras;
  }

  public boolean atualizarValor(int id, double valor) {
    ContentValues values = new ContentValues();
    values.put("valor", valor);

    int atualiza = sqLiteDatabase.update(Database.TABELA_COMPRA, values, "id=" + id, null);

    if (atualiza > 0) {
      return true;
    }
    return false;
  }
}
