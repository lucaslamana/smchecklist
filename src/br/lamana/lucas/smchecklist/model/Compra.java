package br.lamana.lucas.smchecklist.model;

import java.io.Serializable;

/**
 * Representa uma compra.
 */

public class Compra implements Serializable {

  private static final long serialVersionUID = 1L;

  private int id;

  private String titulo;

  private String data;
  
  private double valorLimite;

  public Compra(String titulo, String data, double valorLimite) {
    super();
    this.titulo = titulo;
    this.data = data;
    this.valorLimite = valorLimite;
  }

  public Compra(int id, String titulo, String data, double valorLimite) {
    super();
    this.id = id;
    this.titulo = titulo;
    this.data = data;
    this.valorLimite = valorLimite;
  }

  public Compra() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getId() {
    return id;
  }

  public String getTitulo() {
    return titulo;
  }

  public String getData() {
    return data;
  }

  public double getValorLimite() {
    return valorLimite;
  }

  public void setValorLimite(double valorLimite) {
    this.valorLimite = valorLimite;
  }

  @Override
  public String toString() {
    return new String("id=" + id + ", titulo=" + titulo + ", data=" + data);
  }
}
