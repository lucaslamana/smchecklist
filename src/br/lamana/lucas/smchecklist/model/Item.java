package br.lamana.lucas.smchecklist.model;

public class Item {

  private int id;

  private String descricao;

  private double valor;

  private int quantidade;

  private int compra;

  public Item() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Item(int id, String descricao, double valor, int quantidade, int compra) {
    super();
    this.id = id;
    this.descricao = descricao;
    this.valor = valor;
    this.quantidade = quantidade;
    this.compra = compra;
  }

  public Item(String descricao, double valor, int quantidade, int compra) {
    super();
    this.descricao = descricao;
    this.valor = valor;
    this.quantidade = quantidade;
    this.compra = compra;
  }

  public int getId() {
    return id;
  }

  public String getDescricao() {
    return descricao;
  }

  public double getValor() {
    return valor;
  }

  public int getQuantidade() {
    return quantidade;
  }

  public int getCompra() {
    return compra;
  }

  @Override
  public String toString() {
    return new String("id=" + id + ", descricao=" + descricao + ", valor=" + valor
        + ", quantidade=" + quantidade + ", compra_id=" + compra);
  }
}
