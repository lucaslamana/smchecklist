package br.lamana.lucas.smchecklist;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smcheklist.R;

@SuppressLint({ "InflateParams", "ViewHolder" })
public class AdptadorCompras extends BaseAdapter {

  /**
   * Contexto para, por exemplo, acesso ao {@link LayoutInflater} para inflar o
   * layout a ser retornado no
   * {@link #getView(int, android.view.View, android.view.ViewGroup)}
   */
  private Context context;

  /**
   * Lista de Compras.
   */
  private List<Compra> compras;

  /**
   * Cria um adaptador para Compras.
   * 
   * @param context
   *          Contexto para inflar o layout.
   * 
   * @param compras
   *          Lista de compras.
   */
  public AdptadorCompras(Context context, List<Compra> compras) {
    super();
    this.context = context;
    this.compras = compras;
  }

  @Override
  public int getCount() {
    return compras.size();
  }

  @Override
  public Object getItem(int position) {
    return compras.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // View que será inflada para ser a linha da lista.
    View view = inflater.inflate(R.layout.linha_lista_compras, null);

    Compra compra = compras.get(position);

    // Título da compra.
    TextView textViewTitulo = (TextView) view.findViewById(R.id.linhaCompras_titulo);
    textViewTitulo.setText(compra.getTitulo());

    return view;
  }

  public void setItens(List<Compra> compras) {
    this.compras = compras;
  }
}
