package br.lamana.lucas.smchecklist.util;

import java.util.List;

import br.lamana.lucas.smchecklist.model.Item;

public class Util {


  /**
   * Pega a quantidade de itens da lista.
   * 
   * @param itens
   *          Lista de itens.
   * @return Quantidade de itens.
   */
  public static int getQuantidadeTotal(List<Item> itens) {
    int quantidade = 0;
    for (Item item : itens) {
      quantidade += item.getQuantidade();
    }

    return quantidade;
  }

  /**
   * Calcula o valor total dos produtos que compõe a lista.
   * 
   * @param itens
   *          Lista de itens atual.
   * @return Valor total da compra.
   */
  public static double getValorTotal(List<Item> itens) {
    double valorTotal = 0;
    for (Item item : itens) {
      if (item.getQuantidade() > 1) {
        valorTotal += (item.getValor() * item.getQuantidade());
      }
      else {
        valorTotal += item.getValor();
      }
    }

    return valorTotal;
  }
}
