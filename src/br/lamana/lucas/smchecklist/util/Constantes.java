package br.lamana.lucas.smchecklist.util;

public class Constantes {

  /**
   * Tag para logs.
   */
  public static final String TAG = "[SM]";

  /**
   * Extra de compra.
   */
  public static final String EXTRA_COMPRA = "compra";
  
}
