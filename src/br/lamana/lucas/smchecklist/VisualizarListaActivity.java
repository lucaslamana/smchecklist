package br.lamana.lucas.smchecklist;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.lamana.lucas.smchecklist.database.CompraDAO;
import br.lamana.lucas.smchecklist.database.ItemDAO;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.util.Constantes;
import br.lamana.lucas.smcheklist.R;

public class VisualizarListaActivity extends ActionBarActivity {

  /**
   * ListView de compras.
   */
  private ListView listViewCompras;

  /**
   * Adaptador para a lista de compras.
   */
  private AdptadorCompras adptadorCompras;

  /**
   * DAO de compra para acesso e manipulação do banco de dados.
   */
  private CompraDAO compraDAO;

  /**
   * DAO de compra para acesso e manipulação do banco de dados.
   */
  private ItemDAO itemDAO;

  /**
   * Lista de compras.
   */
  private List<Compra> compras;

  /**
   * Lista de compras quando aplicado o filtro.
   */
  private List<Compra> comprasFiltradas;

  // Opções da lista
  private static final String OPCAO_ALTERAR = "Alterar";
  private static final String OPCAO_EXCLUIR = "Excluir";

  /**
   * Lista com as opções de gerenciamento da compra.
   */
  private String[] opcoes;

  /**
   * EditText para procurar na lista.
   */
  private EditText editTextProcura;

  /**
   * TextWatcher para {@link #editTextProcura}
   */
  private TextWatcher textWatcher;

  /**
   * TextView para informar que não existe nenhuma compra na base de dados.
   */
  private TextView textViewNenhumaCompra;

  /**
   * Boolean para indicar se o filtro foi utilizado ou não.
   */
  private boolean filtrado = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_visualizar_lista);

    compraDAO = new CompraDAO();
    compraDAO.abrir(this);

    itemDAO = new ItemDAO();
    itemDAO.abrir(this);

    compras = compraDAO.listar();
    comprasFiltradas = new ArrayList<Compra>();

    adptadorCompras = new AdptadorCompras(this, compras);

    textViewNenhumaCompra = (TextView) findViewById(R.id.visualizarLista_textViewNenhumaCompra);
    if (compras.size() < 1) {
      textViewNenhumaCompra.setVisibility(View.VISIBLE);
    }

    opcoes = new String[] { OPCAO_ALTERAR, OPCAO_EXCLUIR };

    listViewCompras = (ListView) findViewById(R.id.visualizarLista_listViewCompras);
    listViewCompras.setAdapter(adptadorCompras);
    listViewCompras.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Pega o objeto clicado.
        Compra compra = (Compra) adptadorCompras.getItem(position);

        // Passa o objeto compra por extra para a outra activity.
        startActivity(new Intent(VisualizarListaActivity.this, ListaCompraActivity.class).putExtra(
            Constantes.EXTRA_COMPRA, compra));
      }
    });
    listViewCompras.setOnItemLongClickListener(new OnItemLongClickListener() {

      @Override
      public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        final Compra compra = (Compra) adptadorCompras.getItem(position);

        new AlertDialog.Builder(VisualizarListaActivity.this)
            .setTitle(R.string.global_desejaFazer)
            .setItems(opcoes, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                String opcaoEscolhida = opcoes[which];

                // ALTERAR.
                if (opcaoEscolhida.equals(OPCAO_ALTERAR)) {
                  startActivity(new Intent(VisualizarListaActivity.this, NovaListaActivity.class)
                      .putExtra(Constantes.EXTRA_COMPRA, compra));
                }
                // EXCLUIR.
                else if (opcaoEscolhida.equals(OPCAO_EXCLUIR)) {
                  final Compra compra = (Compra) adptadorCompras.getItem(position);

                  // Dialog de confirmação de exclusão.
                  new AlertDialog.Builder(VisualizarListaActivity.this)
                      .setTitle(R.string.deletarLista_dialog_apagar)
                      .setMessage(
                          getResources().getString(R.string.deletarLista_dialog_apagarCompra,
                              compra.getTitulo()))
                      .setPositiveButton(android.R.string.yes, new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          if (compraDAO.deletar(compra.getId())
                              && itemDAO.deletarItensDeCompra(compra.getId())) {
                            Toast.makeText(VisualizarListaActivity.this,
                                R.string.deletarLista_toast_compraApagada,
                                Toast.LENGTH_SHORT).show();

                            // Se já foi aplicado um filtro, então remove o
                            // objeto na lista de filtro.
                            if (filtrado) {
                              comprasFiltradas.remove(position);
                            }
                            else {
                              compras.remove(position);
                            }
                            adptadorCompras.notifyDataSetChanged();

                            // Se não tiver nenhuma compra, mostra a mensagem.
                            if (adptadorCompras.isEmpty()) {
                              textViewNenhumaCompra.setVisibility(View.VISIBLE);
                            }

                          }
                        }
                      })
                      .setNegativeButton(android.R.string.no, new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                        }
                      })
                      .show();
                }
              }
            })
            .show();
        return false;
      }
    });

    textWatcher = new TextWatcher() {

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        filtrar(s.toString());
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    };

    editTextProcura = (EditText) findViewById(R.id.visualizarLista_editTextProcurar);
    editTextProcura.addTextChangedListener(textWatcher);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.visualizar_lista, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if (id == R.id.visualizarLista_menu_procurar) {
      if (editTextProcura.getVisibility() == View.GONE) {
        editTextProcura.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_top);
        editTextProcura.setAnimation(animation);

        mostrarTeclado(editTextProcura);
      }
      else {
        editTextProcura.setVisibility(View.GONE);
        ocultarTeclado(editTextProcura.getWindowToken());
      }
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onDestroy() {
    compraDAO.fechar();
    itemDAO.fechar();

    super.onDestroy();
  }

  /**
   * Oculta o teclado.
   * 
   * @param iBinder
   */
  public void ocultarTeclado(IBinder iBinder) {
    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
  }

  /**
   * Mostra o teclado.
   * 
   * @param view
   */
  public void mostrarTeclado(View view) {
    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    view.requestFocus();
    inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
  }

  /**
   * Filtra a lista de compras com o padrão especificado por parametro.
   * 
   * @param s
   *          Padrão à procurar.
   */
  private void filtrar(String s) {
    // Foi aplicado um filtro.
    filtrado = true;
    comprasFiltradas.clear();

    if (s.length() == 0 || s == null) {
      comprasFiltradas.addAll(compras);
    }
    else {
      Pattern pattern = Pattern.compile(".*" + s + ".*", Pattern.CASE_INSENSITIVE);
      for (Compra compra : compras) {
        if (pattern.matcher(compra.getTitulo()).find()) {
          comprasFiltradas.add(compra);
        }
      }
    }

    adptadorCompras.setItens(comprasFiltradas);
    adptadorCompras.notifyDataSetChanged();
  }
}
