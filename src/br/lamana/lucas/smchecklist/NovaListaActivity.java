package br.lamana.lucas.smchecklist;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.lamana.lucas.smchecklist.database.CompraDAO;
import br.lamana.lucas.smchecklist.database.ItemDAO;
import br.lamana.lucas.smchecklist.model.Compra;
import br.lamana.lucas.smchecklist.model.Item;
import br.lamana.lucas.smchecklist.util.Constantes;
import br.lamana.lucas.smchecklist.util.Util;
import br.lamana.lucas.smcheklist.R;

@SuppressLint("InflateParams")
public class NovaListaActivity extends ActionBarActivity {

  /**
   * DAO (Data access object) para acesso e manipulação do banco de dados.
   */
  private ItemDAO itemDAO;

  /**
   * ListView com os produtos adicionados.
   */
  private ListView listViewProdutosAdicionados;

  /**
   * EditText para a inserção da descrição do produto.
   */
  private EditText editTextDescricao;

  /**
   * EditText para a inserção do valor do produto.
   */
  private EditText editTextValor;

  /**
   * EditText para inserir a quantidade de itens de um determinado produto.
   */
  private EditText editTextQuantidade;

  /**
   * TextView com o total de produtos e o valor dos mesmo.
   */
  private TextView textViewTotalEValor;

  /**
   * Lista de itens de uma compra.
   */
  private List<Item> itens;

  /**
   * Lista de itens para comparar com {@link #itens} para ver se algum item foi
   * alterado.
   */
  private List<Item> itensInalterados;

  /**
   * Adaptador para a listagem de itens.
   */
  private AdptadorItens adptadorItens;

  /**
   * DAO de compra para acesso e manipulação do banco de dados.
   */
  private CompraDAO compraDAO;

  /**
   * Objeto compra que será recebido por extra.
   */
  private Compra compra;

  /**
   * ActionBar para a manipulação da barra de ações.
   */
  private ActionBar actionBar;

  // Opções da dialog para gerenciar o item.
  private static final String ITEM_ALTERAR = "Alterar";
  private static final String ITEM_APAGAR = "Apagar";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_nova_lista);

    itemDAO = new ItemDAO();
    itemDAO.abrir(this);

    compraDAO = new CompraDAO();
    compraDAO.abrir(this);

    // Recebe a compra da activity anterior.
    compra = (Compra) getIntent().getSerializableExtra(Constantes.EXTRA_COMPRA);

    actionBar = getSupportActionBar();
    actionBar.setSubtitle(compra.getTitulo());

    // Verifica se a compra já possui uma lista de itens (EDITAR)
    itens = itemDAO.listarItensDeCompra(compra.getId());
    adptadorItens = new AdptadorItens(this, itens);

    if (itens.size() > 0) {
      itensInalterados = new ArrayList<Item>();
      itensInalterados.addAll(itens);
    }
    listViewProdutosAdicionados = (ListView) findViewById(R.id.novaLista_listViewProdutosAdicionados);
    listViewProdutosAdicionados.setAdapter(adptadorItens);

    listViewProdutosAdicionados.setOnItemLongClickListener(new OnItemLongClickListener() {

      @Override
      public boolean onItemLongClick(AdapterView<?> parentView, View view, final int position,
          long id) {
        final Item item = (Item) adptadorItens.getItem(position);
        final String[] opcoes = new String[] { ITEM_ALTERAR, ITEM_APAGAR };

        new AlertDialog.Builder(NovaListaActivity.this)
            .setTitle(R.string.global_desejaFazer)
            .setItems(opcoes, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                String opcaoSelecionada = opcoes[which];

                if (opcaoSelecionada.equals(ITEM_ALTERAR)) {
                  LayoutInflater inflater = LayoutInflater.from(NovaListaActivity.this);
                  View v = inflater.inflate(R.layout.dialog_alterar_item, null);

                  // Descrição.
                  final EditText editTextNovaDescricao = (EditText) v
                      .findViewById(R.id.dialog_alterar_editTextDescricao);
                  editTextNovaDescricao.setText(item.getDescricao());

                  // Valor.
                  final EditText editTextNovoValor = (EditText) v
                      .findViewById(R.id.dialog_alterar_editTextValor);
                  editTextNovoValor.setText(String.valueOf(item.getValor()));

                  // Quantidade.
                  final EditText editTextNovaQuantidade = (EditText) v
                      .findViewById(R.id.dialog_alterar_editTextQuantidade);
                  editTextNovaQuantidade.setText(String.valueOf(item.getQuantidade()));

                  new AlertDialog.Builder(NovaListaActivity.this)
                      .setTitle(R.string.dialog_alterar_titulo)
                      .setView(v)
                      .setPositiveButton(android.R.string.ok, new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          String novaDescricao = editTextNovaDescricao.getText().toString();
                          // Verifica de a descrição do item está vazia.
                          if (novaDescricao == null || novaDescricao.equals("")) {
                            Toast.makeText(NovaListaActivity.this,
                                R.string.novaLista_toast_necessarioPreenchimento,
                                Toast.LENGTH_SHORT).show();
                          }
                          else {
                            // Se não estiver, vai verificar se foi digitado um
                            // número válido.
                            try {
                              double novoValor = Double.parseDouble(editTextNovoValor.getText()
                                  .toString());
                              int novaQuantidade = Integer.parseInt(editTextNovaQuantidade
                                  .getText()
                                  .toString());

                              Item novoItem = new Item(novaDescricao, novoValor, novaQuantidade,
                                  compra
                                      .getId());

                              alterar(position, novoItem);
                            }
                            // Se não for um número válido.
                            catch (NumberFormatException nfe) {
                              Toast.makeText(NovaListaActivity.this,
                                  R.string.novaLista_toast_necessarioPreenchimento,
                                  Toast.LENGTH_SHORT).show();
                            }
                          }
                        }
                      })
                      .setNegativeButton(android.R.string.cancel, new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                        }
                      })
                      .show();

                }
                else if (opcaoSelecionada.equals(ITEM_APAGAR)) {
                  apagar(position);
                }
              }
            })
            .show();
        return false;
      }
    });

    editTextDescricao = (EditText) findViewById(R.id.novaLista_editTextDescricao);
    editTextValor = (EditText) findViewById(R.id.novaLista_editTextValor);
    editTextQuantidade = (EditText) findViewById(R.id.novaLista_editTextQuantidade);
    editTextQuantidade.setText(String.valueOf(1));

    textViewTotalEValor = (TextView) findViewById(R.id.novaLista_textViewQuantidadeETotal);
    atualizaTotalEValor(itens);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.nova_lista, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if (id == R.id.novaLista_menu_add) {
      String descricao = editTextDescricao.getText().toString();

      if (descricao != null && !descricao.equals("")) {

        // try-catch para não aceitar o valor do campo nulo.
        try {
          double valor = Double.parseDouble(editTextValor.getText().toString());
          int quantidade = Integer.parseInt(editTextQuantidade.getText().toString());

          final Item i = new Item(descricao, valor, quantidade, compra.getId());

          double valorAtual = Util.getValorTotal(itens) + (quantidade * valor);

          if (valorAtual > compra.getValorLimite()) {
            new AlertDialog.Builder(this)
                .setTitle("Atenção")
                .setMessage(
                    "O valor da compra está ultrapassando o limite estipulado. O que deseja fazer?")
                .setNegativeButton("Não adicionar", new DialogInterface.OnClickListener() {

                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                  }
                })
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {

                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    itens.add(i);
                  }
                })
                .setNeutralButton("Alterar limite", new DialogInterface.OnClickListener() {

                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    alterarLimiteCompra();
                  }
                })
                .show();
          }
          else {
            itens.add(i);
          }
          adptadorItens.notifyDataSetChanged();

          atualizaTotalEValor(itens);

          reiniciaCampos();
        }
        catch (NumberFormatException nfe) {
          Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_insiraValor,
              Toast.LENGTH_SHORT).show();
        }
      }
      else {
        Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_insiraNome,
            Toast.LENGTH_SHORT).show();
      }
    }
    else if (id == R.id.novaLista_menu_fecharCompra) {
      if (itens.size() > 0) {
        new AlertDialog.Builder(this)
            .setTitle(R.string.novaLista_dialog_fecharCompra)
            .setMessage(R.string.novaLista_dialog_finalizarCompra)
            .setPositiveButton(android.R.string.yes, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                gravaLista(itens);
                Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_listaSalva,
                    Toast.LENGTH_SHORT).show();

                finish();
              }
            })
            .setNegativeButton(android.R.string.no, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
            }).show();
      }
      else {
        Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_listaVazia,
            Toast.LENGTH_SHORT).show();
      }
    }
    else if (id == R.id.novaLista_menu_limparLista) {
      if (itens.size() > 0) {
        new AlertDialog.Builder(this)
            .setTitle(R.string.novaLista_dialog_limparLista)
            .setMessage(R.string.novaLista_dialog_desejaLimparLista)
            .setPositiveButton(android.R.string.yes, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                itens.clear();
                adptadorItens.notifyDataSetChanged();

                atualizaTotalEValor(itens);
                reiniciaCampos();

                Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_apagadoComSucesso,
                    Toast.LENGTH_SHORT).show();
              }
            })
            .setNegativeButton(android.R.string.no, new OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
              }
            })
            .show();
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onDestroy() {
    itemDAO.fechar();
    compraDAO.fechar();
    super.onDestroy();
  }

  /**
   * Atualiza {@link #textViewTotalEValor} com o total de produtos e o valor
   * total da lista de compras atual.
   * 
   * @param itens
   *          lista de itens atual.
   */
  private void atualizaTotalEValor(List<Item> itens) {
    StringBuilder builder = new StringBuilder();
    int quantidade = 0;

    for (Item item : itens) {
      quantidade += item.getQuantidade();
    }

    double valorTotal = Util.getValorTotal(itens);

    builder.append(quantidade);
    builder.append(" itens, R$");
    builder.append(String.format("%.2f", valorTotal) + ", de R$");
    builder.append(String.format("%.2f", compra.getValorLimite()));

    textViewTotalEValor.setText(builder.toString());

    if (valorTotal > compra.getValorLimite()) {
      textViewTotalEValor.setTextColor(getResources().getColor(R.color.vermelho));
    }
    else {
      textViewTotalEValor.setTextColor(getResources().getColor(android.R.color.darker_gray));
    }
  }

  @Override
  public void onBackPressed() {
    // Se tiver algum item na lista de compras, cria um alerta para que o
    // usuário possa salvar a compra.
    if (itens.size() > 0 && !itens.equals(itensInalterados)) {
      new AlertDialog.Builder(this)
          .setTitle(R.string.novaLista_dialog_desejaSalvaritens)
          .setMessage(R.string.novaLista_dialog_salvarItens)
          .setPositiveButton(android.R.string.yes, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
              gravaLista(itens);
              Toast.makeText(NovaListaActivity.this, R.string.novaLista_toast_listaSalva,
                  Toast.LENGTH_SHORT).show();

              finish();
            }
          })
          .setNegativeButton(android.R.string.no, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
              finish();
            }
          })
          .show();
    }
    else {
      compraDAO.deletar(compra.getId());
      Toast.makeText(this, R.string.novaLista_toast_listaNaoSalva, Toast.LENGTH_SHORT).show();
      super.onBackPressed();
    }
  }

  /**
   * Apaga da lista local o item selecionado.
   * 
   * @param position
   *          Item selecionado
   */
  private void apagar(int position) {
    itens.remove(position);
    adptadorItens.notifyDataSetChanged();
    atualizaTotalEValor(itens);
  }

  /**
   * Atualiza o item selecionado
   * 
   * @param position
   *          Item selecionado.
   * 
   * @param item
   *          Item alterado.
   */
  private void alterar(int position, Item item) {
    itens.set(position, item);
    adptadorItens.notifyDataSetChanged();
    atualizaTotalEValor(itens);
  }

  /**
   * Grava a lista de itens adicionados na compra no banco de dados.
   * 
   * @param itens
   *          Lista de itens atual.
   */
  private void gravaLista(List<Item> itens) {
    // Deleta todos os itens da compra para evitar duplicação de produtos (NO
    // CASO DE EDIÇÃO).
    if (itemDAO.listarItensDeCompra(compra.getId()).size() > 0) {
      itemDAO.deletarItensDeCompra(compra.getId());
    }
    for (Item item : itens) {
      itemDAO.inserir(item.getDescricao(), item.getValor(), item.getQuantidade(), compra.getId());
    }
  }

  /**
   * Apaga o valor dos campos e coloca o foco em {@link #editTextDescricao}
   * assim que é clicado no botão de adicionar item.
   */
  private void reiniciaCampos() {
    editTextDescricao.setText("");
    editTextValor.setText("");
    editTextQuantidade.setText(String.valueOf(1));

    editTextDescricao.requestFocus();
  }

  private void alterarLimiteCompra() {
    View view = getLayoutInflater().inflate(R.layout.dialog_edit_text, null);

    EditText editTextTitulo = (EditText) view.findViewById(R.id.dialog_editTextTitulo);
    editTextTitulo.setText(compra.getTitulo());
    editTextTitulo.setEnabled(false);

    final EditText editTextValorLimite = (EditText) view
        .findViewById(R.id.dialog_alterar_editTextValor);
    editTextValorLimite.setText(String.valueOf(compra.getValorLimite()));

    new AlertDialog.Builder(this)
        .setTitle("Alterar limite da compra")
        .setView(view)
        .setPositiveButton("Alterar", new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            double valorLimite = Double.parseDouble(editTextValorLimite.getText().toString());

            if (valorLimite != 0) {
              compra.setValorLimite(valorLimite);
              compraDAO.atualizarValor(compra.getId(), valorLimite);
            }
          }
        })
        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {

          }
        }).show();
  }
}
