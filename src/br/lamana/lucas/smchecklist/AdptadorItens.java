package br.lamana.lucas.smchecklist;

import java.util.List;

import br.lamana.lucas.smchecklist.model.Item;
import br.lamana.lucas.smcheklist.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

@SuppressLint({ "InflateParams", "ViewHolder" })
public class AdptadorItens extends BaseAdapter {

  /**
   * Contexto para, por exemplo, acesso ao {@link LayoutInflater} para inflar o
   * layout a ser retornado no
   * {@link #getView(int, android.view.View, android.view.ViewGroup)}
   */
  private Context context;

  /**
   * Itens da lista de compras.
   */
  private List<Item> itens;

  /**
   * Cria um adaptador para Itens.
   * 
   * @param context
   *          Contexto para inflar o layout
   * @param itens
   *          Itens da lista de compras.
   */
  public AdptadorItens(Context context, List<Item> itens) {
    super();
    this.context = context;
    this.itens = itens;
  }

  @Override
  public int getCount() {
    return itens.size();
  }

  @Override
  public Object getItem(int position) {
    return itens.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // View a ser inflada para ser a linha da lista.
    View view = inflater.inflate(R.layout.linha_lista_itens, null);

    Item item = itens.get(position);

    // Descricao do item
    TextView textViewDescricao = (TextView) view.findViewById(R.id.linhaItens_descricao);
    textViewDescricao.setText(item.getDescricao());

    // Valor do item
    TextView textViewValor = (TextView) view.findViewById(R.id.linhaItens_valor);
    textViewValor.setText(String.valueOf(String.format("R$ %.2f", item.getValor())));

    // Somente de a quantidade do item for maior que 1 aparecerá a quantidade e
    // o valor total do mesmo.
    if (item.getQuantidade() > 1) {
      // Quantidade do item
      TextView textViewQuantidade = (TextView) view.findViewById(R.id.linhaItens_quantidade);
      textViewQuantidade.setText(String.valueOf("(" + item.getQuantidade() + "x)"));

      // Valor total dos itens com a quantidade maior que 1
      double valorTotal = item.getValor() * item.getQuantidade();

      // Valor total dos itens
      TextView textViewValorTotal = (TextView) view.findViewById(R.id.linhaItens_valorTotal);
      textViewValorTotal.setText(String.valueOf(String.format("= R$ %.2f", valorTotal)));
    }
    return view;
  }

  public void setItens(List<Item> itens) {
    this.itens = itens;
  }
}
